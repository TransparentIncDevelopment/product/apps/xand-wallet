export const TimeoutError = {
  type: "Timeout",
  name: "This is a timeout error",
  message: "Timed out waiting on transaction polling."
};

export const Std500Error = {
  type: "Generic Server Error",
  status: 500,
  name: "This is a server error",
  message: "Server error, could not process request."
};

export const ReserveTransferError = {
  type: "Reserve Transfer Error",
  name: "Some Bank Reserve Transfer Error",
  message: "Could not make reserve transfer from bank."
};

export const RedeemExcessFunds = {
  type: "Redeem Request Exceeds Error",
  name: "Some Redeem Overdraw Error",
  message: "Redeem Amount Exceeds Wallet Funds"
};

import { injectable } from "inversify-props";

@injectable()
export default class InMemoryStorage implements Storage {
  private map = new Map<string, string>();

  public get length(): number {
    return this.map.size;
  }

  public clear(): void {
    this.map.clear();
  }

  public getItem(key: string): string | null {
    const returnValue = this.map.get(key);
    return returnValue ? returnValue : null;
  }

  public key(index: number): string | null {
    const iterator = this.map.keys();
    let i = 1;
    let iteration = iterator.next();
    if (iteration.done) {
      return null;
    }
    while (i <= index && !iteration.done) {
      iteration = iterator.next();
      if (iteration.done) {
        return null;
      }
      i++;
    }

    return iteration.value;
  }

  public removeItem(key: string): void {
    this.map.delete(key);
  }

  public setItem(key: string, value: string): void {
    this.map.set(key, value);
  }
}

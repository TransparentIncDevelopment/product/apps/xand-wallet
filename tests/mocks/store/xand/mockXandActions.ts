const mock: {
  getTransactionHistoryPage: () => Promise<void>;
  getExportableTransactions: () => Promise<void>;
  refreshWalletBalance: () => Promise<void>;
} = {
  getTransactionHistoryPage: () => {
    return new Promise((res, _) => res());
  },
  getExportableTransactions: () => {
    return new Promise((res, _) => res());
  },
  refreshWalletBalance: () => {
    return new Promise((res, _) => res());
  }
};

export default mock;

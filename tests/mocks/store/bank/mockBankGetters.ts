import BankStore from "@/store/modules/bank";
import {
  SingleBankSingleAccountCollection,
  SingleBankMultipleAccountCollection,
  MultipleBankSingleAccountCollection,
  MultipleBankMultipleAccountCollection,
  AccountOne,
  AccountFive,
  AllBanksById
} from "../../data/mockBankAccounts";

export const SingleBankSingleAccountGetters: Partial<BankStore> = {
  allAccounts: SingleBankSingleAccountCollection,
  currentAccount: SingleBankSingleAccountCollection.values().next().value,
  currentAccountId: SingleBankSingleAccountCollection.values().next().value.id,
  accountById: (id: number) => SingleBankSingleAccountCollection.get(id),
  allBanks: AllBanksById
};

export const SingleBankMultipleAccountGetters: Partial<BankStore> = {
  allAccounts: SingleBankMultipleAccountCollection,
  currentAccount: SingleBankMultipleAccountCollection.get(AccountOne.id),
  currentAccountId: AccountOne.id,
  accountById: (id: number) => SingleBankMultipleAccountCollection.get(id),
  allBanks: AllBanksById
};

export const MultipleBankSingleAccountGetters: Partial<BankStore> = {
  allAccounts: MultipleBankSingleAccountCollection,
  currentAccount: MultipleBankSingleAccountCollection.get(AccountOne.id),
  currentAccountId: AccountOne.id,
  accountById: (id: number) => MultipleBankSingleAccountCollection.get(id),
  allBanks: AllBanksById
};

export const MultipleBankMultipleAccountGetters: Partial<BankStore> = {
  allAccounts: MultipleBankMultipleAccountCollection,
  currentAccount: MultipleBankMultipleAccountCollection.get(AccountFive.id),
  currentAccountId: AccountFive.id,
  accountById: (id: number) => MultipleBankMultipleAccountCollection.get(id),
  allBanks: AllBanksById
};

export const MultipleBankMultipleAccountDefaultNotSetGetters: Partial<BankStore> = {
  allAccounts: MultipleBankMultipleAccountCollection,
  currentAccount: undefined,
  currentAccountId: -1,
  accountById: (id: number) => MultipleBankMultipleAccountCollection.get(id),
  allBanks: AllBanksById
};

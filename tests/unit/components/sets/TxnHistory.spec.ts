import { Wrapper, shallowMount } from "@vue/test-utils";
import { expect } from "chai";

import {
  NoTransactionsGetters,
  AllTransactionsGetters
} from "../../../mocks/store/xand/mockXandGetters";
import mockXandActions from "../../../mocks/store/xand/mockXandActions";
import {
  ConfirmedCreateRequest,
  ConfirmedRedeemRequest,
  PendingCreateRequest,
  CancelledCreateRequest,
  PendingRedeemRequest,
  CancelledRedeemRequest,
  PaymentSent,
  PaymentReceived,
  UnsupportedTransaction
} from "../../../mocks/data/mockTransactions";
import TxnHistory from "@/components/sets/TxnHistory.vue";
import displayMessages from "@/constants/displayMessages";

describe("Transaction History Table", () => {
  const noTxWrapper: Wrapper<TxnHistory> = shallowMount(TxnHistory, {
    computed: {
      xandModule: () => ({ ...mockXandActions, ...NoTransactionsGetters })
    }
  });
  const allTxWrapper: Wrapper<TxnHistory> = shallowMount(TxnHistory, {
    computed: {
      xandModule: () => ({ ...mockXandActions, ...AllTransactionsGetters })
    }
  });

  describe("Component Mounting", () => {
    it("mounts", () => {
      expect(noTxWrapper.exists()).to.be.true;
      expect(noTxWrapper.is(TxnHistory)).to.be.true;
    });

    it("mounts with transactions", () => {
      expect(allTxWrapper.exists()).to.be.true;
      expect(allTxWrapper.is(TxnHistory)).to.be.true;
    });

    it("Displays a message if there are no transactions", () => {
      expect(noTxWrapper.find("tr > td > span").text()).to.equal(
        "No transactions yet."
      );
    });

    it("Displays a row for every transaction", () => {
      const rows = allTxWrapper.findAll("tbody > tr");
      expect(rows.length).to.equal(
        AllTransactionsGetters.transactionView.length
      );
    });
  });

  describe("'To/From' Column", () => {
    it("Displays a formatted To/From", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.ToFrom,
          ConfirmedCreateRequest.transactionId
        ).text
      ).to.equal(ConfirmedCreateRequest.toOrFromAddress);
    });

    it("Displays a formatted To/From with contact name", () => {
      expect(
        findCellForId(allTxWrapper, Cells.ToFrom, PaymentSent.transactionId)
          .text
      ).to.equal(PaymentSent.toOrFromContact);
    });
  });

  describe("'Datetime' Column", () => {
    it("Displays a formatted datetime", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Datetime,
          ConfirmedCreateRequest.transactionId
        ).text
      ).to.equal(ConfirmedCreateRequest.formattedDatetime);
    });
  });

  describe("'Value' Column", () => {
    it("Displays a formatted positive amount", () => {
      const cell = findCellForId(
        allTxWrapper,
        Cells.Value,
        ConfirmedCreateRequest.transactionId
      );
      const icon = findValueIconWrapper(cell.wrapper);
      expect(icon.classes().includes("icon-v-add")).to.be.true;
      expect(cell.text).to.equal("$0.11");
    });

    it("Displays a formatted negative amount", () => {
      const cell = findCellForId(
        allTxWrapper,
        Cells.Value,
        ConfirmedRedeemRequest.transactionId
      );
      const icon = findValueIconWrapper(cell.wrapper);
      expect(icon.classes().includes("icon-v-deduct")).to.be.true;
      expect(cell.text).to.equal("$0.11");
    });
  });

  describe("'Type' Column", () => {
    it("Displays proper type for pending creations", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Type,
          PendingCreateRequest.transactionId
        ).text
      ).to.equal(displayMessages.txHistory.transferPending);
    });

    it("Displays proper type for confirmed creations", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Type,
          ConfirmedCreateRequest.transactionId
        ).text
      ).to.equal(displayMessages.txHistory.transferToWallet);
    });

    it("Displays proper type for cancelled creations", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Type,
          CancelledCreateRequest.transactionId
        ).text
      ).to.equal(displayMessages.txHistory.transferCancelled);
    });

    it("Displays proper type for pending redeems", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Type,
          PendingRedeemRequest.transactionId
        ).text
      ).to.equal(displayMessages.txHistory.transferPending);
    });

    it("Displays proper type for confirmed redeems", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Type,
          ConfirmedRedeemRequest.transactionId
        ).text
      ).to.equal(displayMessages.txHistory.transferToBank);
    });

    it("Displays proper type for cancelled redeems", () => {
      expect(
        findCellForId(
          allTxWrapper,
          Cells.Type,
          CancelledRedeemRequest.transactionId
        ).text
      ).to.equal(displayMessages.txHistory.transferCancelled);
    });

    it("Displays proper type for sent payments", () => {
      expect(
        findCellForId(allTxWrapper, Cells.Type, PaymentSent.transactionId).text
      ).to.equal(displayMessages.txHistory.paymentSent);
    });

    it("Displays proper type for received payments", () => {
      expect(
        findCellForId(allTxWrapper, Cells.Type, PaymentReceived.transactionId)
          .text
      ).to.equal(displayMessages.txHistory.paymentReceived);
    });

    it("Handles enriched unsupported transaction types", () => {
      const targetTxId = UnsupportedTransaction.transactionId;
      expect(findCellForId(allTxWrapper, Cells.Type, targetTxId).text).to.equal(
        "N/A"
      );
    });
  });
});

enum Cells {
  ToFrom,
  Datetime,
  Value,
  Type
}

interface Cell {
  wrapper: Wrapper<Vue>;
  text: string;
}

function findRowForId(
  wrapper: Wrapper<TxnHistory>,
  txId: string
): Wrapper<Vue> {
  return wrapper.find(`[id="` + txId + `"]`);
}

function findCellForId(
  wrapper: Wrapper<TxnHistory>,
  cell: Cells,
  txId: string
): Cell {
  const row = findRowForId(wrapper, txId);

  switch (cell) {
    case Cells.ToFrom:
      return findToFromCell(row);
    case Cells.Datetime:
      return findDatetimeCell(row);
    case Cells.Value:
      return findValueCell(row);
    case Cells.Type:
      return findTypeCell(row);
  }
}

function findDatetimeCell(row: Wrapper<Vue>): Cell {
  const datetimeCell = row.find(`[name="datetime"]`);
  const datetimeString = datetimeCell.find("div");
  return { wrapper: datetimeCell, text: datetimeString.text() };
}

function findToFromCell(row: Wrapper<Vue>): Cell {
  const tofromCell = row.find(`[name="tofrom"]`);
  const tofromString = tofromCell.find("div");
  return { wrapper: tofromCell, text: tofromString.text() };
}

function findValueCell(row: Wrapper<Vue>): Cell {
  const valueCell = row.find(`[name="value"]`);
  const valueString = valueCell.findAll("span").at(1);
  return { wrapper: valueCell, text: valueString.text() };
}

function findTypeCell(row: Wrapper<Vue>): Cell {
  const typeCell = row.find(`[name="type"]`);
  const typeText = typeCell.find("span");
  return { wrapper: typeCell, text: typeText.text() };
}

function findValueIconWrapper(cell: Wrapper<Vue>): Wrapper<Vue> {
  const valueIcon = cell.find("i");
  return valueIcon;
}

import { shallowMount } from "@vue/test-utils";
import { expect } from "chai";

import BankAndAccountSelector from "../../../../src/components/buildings/BankAndAccountSelector.vue";
import BankSelectionDropdown from "../../../../src/components/bricks/BankSelectionDropdown.vue";
import AccountSelectionDropdown from "../../../../src/components/bricks/AccountSelectionDropdown.vue";
import {
  SingleBankSingleAccountGetters,
  MultipleBankMultipleAccountGetters
} from "../../../mocks/store/bank/mockBankGetters";

describe("Bank and Account Selector Component", () => {
  it("mounts", () => {
    const wrapper = shallowMount(BankAndAccountSelector, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters })
      }
    });
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BankAndAccountSelector)).to.be.true;
    expect(wrapper.find(BankSelectionDropdown).exists()).to.be.true;
    expect(wrapper.find(AccountSelectionDropdown).exists()).to.be.true;
  });

  it("Hides if there is only one account", () => {
    const wrapper = shallowMount(BankAndAccountSelector, {
      computed: {
        bankModule: () => ({ ...SingleBankSingleAccountGetters })
      }
    });
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BankAndAccountSelector)).to.be.true;
    expect(wrapper.find(".root").isVisible()).to.be.false;
  });
});

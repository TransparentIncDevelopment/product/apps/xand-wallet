import { shallowMount, mount } from "@vue/test-utils";
import { expect } from "chai";

import BankAccountList from "../../../../src/components/buildings/BankAccountList.vue";
import ClickableHighlightRow from "../../../../src/components/bricks/ClickableHighlightRow.vue";
import {
  SingleBankSingleAccountGetters,
  MultipleBankMultipleAccountGetters
} from "../../../mocks/store/bank/mockBankGetters";
import { AccountTwo } from "../../../mocks/data/mockBankAccounts";

describe("Bank Account List", () => {
  it("mounts", () => {
    const wrapper = shallowMount(BankAccountList, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters })
      }
    });
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BankAccountList)).to.be.true;
  });

  it("Mounts one item for one account", () => {
    const wrapper = shallowMount(BankAccountList, {
      computed: {
        bankModule: () => ({ ...SingleBankSingleAccountGetters })
      }
    });
    expect(wrapper.find(ClickableHighlightRow).exists()).to.be.true;
  });

  it("Mounts an item for each account", () => {
    const wrapper = shallowMount(BankAccountList, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters })
      }
    });
    expect(wrapper.findAll(ClickableHighlightRow).length).to.equal(6);
  });

  it("Emits a select-item event when an item is clicked", async () => {
    const wrapper = mount(BankAccountList, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters })
      }
    });
    const emitted = wrapper.emitted();
    wrapper.find(".label-container").trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted["select-item"]).to.eql([[AccountTwo]]);
  });

  it("Emits a select-item event with a null value when an item is toggled off", async () => {
    const wrapper = mount(BankAccountList, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters })
      }
    });
    const emitted = wrapper.emitted();
    wrapper.find(".label-container").trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted["select-item"]).to.eql([[AccountTwo]]);
    wrapper.find(".label-container").trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted["select-item"]).to.eql([[AccountTwo], [null]]);
  });
});

import { expect } from "chai";
import TransferModal from "@/components/buildings/TransferModal.vue";
import TransferModalTestContext from "./TransferTestContext";
import {
  CreateReceipt,
  CreateRequest,
  RedeemReceipt,
  Redemption,
  ReserveBankTransfer
} from "../../../../mocks/data/mockTransactions";
import {
  ReserveTransferError,
  Std500Error,
  TimeoutError
} from "../../../../mocks/data/mockErrors";
import displayMessages from "@/constants/displayMessages";
import { loggerIndex, loggerMessage } from "@/constants/logger";
import { AccountOne } from "../../../../mocks/data/mockBankAccounts";

describe("Given a transfer modal", () => {
  let context: TransferModalTestContext;

  beforeEach(() => {
    context = new TransferModalTestContext(TransferModal);
  });

  it("And it mounts correctly", () => {
    expect(context.wrapper!.exists()).to.equal(true);
  });

  describe("And a user that makes a transfer from bank to wallet", async () => {
    let emitted: any;
    let creationReqAmtMinorUnits: any;
    let creationReqAmtMinorUnitsExceedsBankBalance: any;
    let transferToWalletUserInput: any;
    let transferToBankUserInputExceedsBalance: any;

    beforeEach(async () => {
      emitted = context.emitted;
      creationReqAmtMinorUnits = context.constructUSDFromMinorUnits(
        CreateRequest.amountInMinorUnit
      );
      // TODO use getters to get the current selected bank balance instead of directly referencing mock - for better separation
      creationReqAmtMinorUnitsExceedsBankBalance = context.constructUSDFromMinorUnits(
        AccountOne.balance.amountInMinorUnits.toNumber() + 1
      );
    });
    describe("When the transfer to wallet is valid", async () => {
      beforeEach(async () => {
        transferToWalletUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "wallet",
          transferAmount: creationReqAmtMinorUnits
        }; // TODO use a mock representing a member not "trustee", name is misleading
        context.setupCreationRequest(CreateRequest, CreateReceipt);
        await context.submitTransferModalUI(transferToWalletUserInput);
      });
      it("Emits one submit event", () => {
        expect(emitted.submit.length).to.eq(1);
      });

      it("Transfers the form data correctly to the API creation request call", () => {
        expect(context.verifyCreationRequest(CreateRequest)).to.be.true;
      });

      it("Triggers pop up alert confirming the transaction has been made successfully", () => {
        expect(
          context.sweetAlertsShouldContain(displayMessages.LOAD_WALLET_SUCCESS)
        ).to.be.true;
      });

      it("Does not show an error message in the modal", () => {
        expect(context.errorMessageShouldBe("")).to.be.true;
      });
    });

    describe("When the requested transfer to wallet exceeds selected bank balance", async () => {
      beforeEach(async () => {
        transferToBankUserInputExceedsBalance = {
          currentSelectedAccount: 1,
          transferDirection: "wallet",
          transferAmount: creationReqAmtMinorUnitsExceedsBankBalance
        };
        await context.submitTransferModalUI(
          transferToBankUserInputExceedsBalance
        );
      });
      it("Triggers pop up alerting that the amount is not valid", () => {
        // Specific error message hard coded as string because its content is a business requirement
        const expectedAlertMsg =
          "Amount entered exceeds available bank balance.";
        expect(context.sweetAlertsShouldContain(expectedAlertMsg)).to.be.true;
      });
      it("Triggers a descriptive error message in the modal", () => {
        expect(
          context.errorMessageShouldBe(displayMessages.INSUFFICIENT_BALANCE)
        ).to.be.true;
      });
      it("Triggers log with a descriptive error message.", async () => {
        expect(
          context.errorLogsShouldContain(loggerMessage.INVALID_TRANSFER_REQUEST)
        ).to.be.true;
      });
    });

    describe("When the create request API call fails", async () => {
      beforeEach(async () => {
        transferToWalletUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "wallet",
          transferAmount: creationReqAmtMinorUnits
        };
        context.setupCreationRequestFailure(CreateRequest, Std500Error);
        await context.submitTransferModalUI(transferToWalletUserInput);
      });
      // Error messages hard coded as string because its content is a business requirement
      it("Triggers a descriptive error message in the modal.", () => {
        const expectedAlertMsg =
          "Unexpected network error. Please try again or contact your system admin.";
        expect(context.errorMessageShouldBe(expectedAlertMsg)).to.be.true;
      });
      it("Triggers log that includes wallet's internal logger index id", async () => {
        const expectedErrorMsgSnippet = loggerIndex.ERR_.SER0051;
        expect(context.errorLogsShouldContain(expectedErrorMsgSnippet)).to.be
          .true;
      });
    });

    describe("When the corresponding reserve transfer fails", async () => {
      beforeEach(async () => {
        transferToWalletUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "wallet",
          transferAmount: creationReqAmtMinorUnits
        };
        context
          .setupCreationRequest(CreateRequest, CreateReceipt)
          .setupReserveTransferFailure(
            ReserveBankTransfer,
            ReserveTransferError
          );
        await context.submitTransferModalUI(transferToWalletUserInput);
      });
      // Error messages hard coded as string because its content is a business requirement
      it("Triggers a descriptive error message in the modal.", () => {
        const expectedErrorMsg = "Could not make reserve bank transfer";
        expect(context.errorMessageShouldBe(expectedErrorMsg)).to.be.true;
      });
    });

    describe("When the trustee polling times out", async () => {
      beforeEach(async () => {
        transferToWalletUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "wallet",
          transferAmount: creationReqAmtMinorUnits
        };
        context
          .setupCreationRequest(CreateRequest, CreateReceipt)
          .setupPollingFailure(CreateReceipt, TimeoutError);
        await context.submitTransferModalUI(transferToWalletUserInput);
      });
      // Error messages hard coded as string because its content is a business requirement
      it("Triggers pop up alert with a user-friendly error message", () => {
        const expectedAlertMsg =
          "Could not refresh balances. Please contact support@xand.me";
        expect(context.sweetAlertsShouldContain(expectedAlertMsg)).to.be.true;
      });
      it("Triggers log with a descriptive error message.", async () => {
        const expectedErrorMsg = "Timeout while updating both account balances";
        expect(context.errorLogsShouldContain(expectedErrorMsg)).to.be.true;
      });
    });
  });

  describe("And a user that makes a transfer from wallet to bank", () => {
    let emitted: any;
    let redeemAmtInMinorUnits: any;
    let redeemAmtInMinorUnitsExceedsWalletBal: any;
    let transferToBankUserInput: any;
    let transferToBankUserInputExceedsBalance: any;

    beforeEach(async () => {
      emitted = context.emitted;
      redeemAmtInMinorUnits = context.constructUSDFromMinorUnits(
        Redemption.amountInMinorUnit
      );
      redeemAmtInMinorUnitsExceedsWalletBal = context.constructUSDFromMinorUnits(
        (context.wrapper!.vm.viewBalanceInMinorUnit as number) + 1
      );
    });

    describe("When the transfer to bank is valid", async () => {
      beforeEach(async () => {
        transferToBankUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "bank",
          transferAmount: redeemAmtInMinorUnits
        };
        context.setupRedeem(Redemption, RedeemReceipt);
        await context.submitTransferModalUI(transferToBankUserInput);
      });
      it("Emits one submit event", async () => {
        emitted = context.emitted;
        expect(emitted.submit.length).to.eq(1);
      });
      it("Transfers the form data correctly to the API creation request call", () => {
        expect(context.verifyRedeem(Redemption)).to.be.true;
      });
      it("Triggers pop up alert confirming the transaction has been made successfully", () => {
        expect(
          context.sweetAlertsShouldContain(
            displayMessages.REDEEM_WALLET_SUCCESS
          )
        ).to.be.true;
      });
      it("Does not show an error message in the modal", () => {
        expect(context.errorMessageShouldBe("")).to.be.true;
      });
    });

    describe("When the requested transfer to bank exceeds wallet balance", async () => {
      beforeEach(async () => {
        transferToBankUserInputExceedsBalance = {
          currentSelectedAccount: 1,
          transferDirection: "bank",
          transferAmount: redeemAmtInMinorUnitsExceedsWalletBal
        };
        // context.setupRedeemFailure(Redemption, RedeemExcessFunds);
        await context.submitTransferModalUI(
          transferToBankUserInputExceedsBalance
        );
      });
      it("Triggers pop up alerting that the amount is not valid", () => {
        // Specific error message hard coded as string because its content is a business requirement
        const expectedAlertMsg =
          "Amount entered exceeds available wallet balance.";
        expect(context.sweetAlertsShouldContain(expectedAlertMsg)).to.be.true;
      });
      it("Triggers a descriptive error message in the modal", () => {
        expect(
          context.errorMessageShouldBe(displayMessages.INSUFFICIENT_BALANCE)
        ).to.be.true;
      });
      it("Triggers log with a descriptive error message.", async () => {
        expect(
          context.errorLogsShouldContain(loggerMessage.INVALID_TRANSFER_REQUEST)
        ).to.be.true;
      });
    });

    describe("When the redeem API call fails", async () => {
      beforeEach(async () => {
        transferToBankUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "bank",
          transferAmount: redeemAmtInMinorUnits
        };
        context.setupRedeemFailure(Redemption, Std500Error);
        await context.submitTransferModalUI(transferToBankUserInput);
      });
      // Error messages hard coded as string because its content is a business requirement
      it("Triggers a descriptive error message in the modal.", () => {
        const expectedAlertMsg =
          "Unexpected network error. Please try again or contact your system admin.";
        expect(context.errorMessageShouldBe(expectedAlertMsg)).to.be.true;
      });
      it("Triggers log that includes wallet's internal logger index id", async () => {
        const expectedErrorMsgSnippet = loggerIndex.ERR_.SER0050;
        expect(context.errorLogsShouldContain(expectedErrorMsgSnippet)).to.be
          .true;
      });
    });

    describe("When the trustee polling times out", async () => {
      beforeEach(async () => {
        transferToBankUserInput = {
          currentSelectedAccount: 1,
          transferDirection: "bank",
          transferAmount: redeemAmtInMinorUnits
        };
        context
          .setupRedeem(Redemption, RedeemReceipt)
          .setupPollingFailure(RedeemReceipt, TimeoutError);
        await context.submitTransferModalUI(transferToBankUserInput);
      });
      // Error messages hard coded as string because its content is a business requirement
      it("Triggers pop up alert with a user-friendly error message", () => {
        const expectedAlertMsg =
          "Could not refresh balances. Please contact support@xand.me";
        expect(context.sweetAlertsShouldContain(expectedAlertMsg)).to.be.true;
      });
      it("Triggers log with a descriptive error message.", async () => {
        const expectedErrorMsg = "Timeout while updating both account balances";
        expect(context.errorLogsShouldContain(expectedErrorMsg)).to.be.true;
      });
    });
  });
});

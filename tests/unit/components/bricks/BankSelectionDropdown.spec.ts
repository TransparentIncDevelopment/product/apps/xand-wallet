import { mount, Wrapper } from "@vue/test-utils";
import { expect } from "chai";

import BankSelectionDropdown from "../../../../src/components/bricks/BankSelectionDropdown.vue";
import { MultipleBankMultipleAccountGetters } from "../../../mocks/store/bank/mockBankGetters";
import registerBaseComponents from "../base/registerBaseComponents";

const localVue = registerBaseComponents();

describe("Bank Selection Dropdown Component", () => {
  let wrapper: Wrapper<any>;

  beforeEach(() => {
    wrapper = mount(BankSelectionDropdown, {
      computed: {
        bankModule: () => ({ ...MultipleBankMultipleAccountGetters })
      },
      localVue
    });
  });

  it("mounts", () => {
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BankSelectionDropdown)).to.be.true;
  });

  it("populates options for all banks of owned accounts except the current selection", () => {
    expect(wrapper.findAll(".dropdown-item").length).to.equal(1);
    expect(wrapper.find(`[value='Octopoda Bank']`).exists()).to.be.true;
  });

  it("auto-selects expected bank", () => {
    expect(wrapper.find(".selection").text()).to.equal("Chambered Trust");
  });

  it("changes selection and emits bank-selection event on option click", async () => {
    const emitted = wrapper.emitted();
    expect(wrapper.find(`.selection`).text()).to.equal("Chambered Trust");
    wrapper.find(`#dropdownMenuLink`).trigger("click");
    await wrapper.vm.$nextTick();
    wrapper.find(`[value='Octopoda Bank']`).trigger("click");
    await wrapper.vm.$nextTick();
    expect(wrapper.find(`.selection`).text()).to.equal("Octopoda Bank");
    // Bank selection event for mounting with default, and then for a new selection.
    expect(emitted["bank-selection"]).to.eql([[2], [1]]);
  });
});

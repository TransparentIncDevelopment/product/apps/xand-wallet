import { shallowMount, mount } from "@vue/test-utils";
import { expect } from "chai";

import WalletAddressInput from "../../../../src/components/bricks/WalletAddressInput.vue";
import registerBaseComponents from "../base/registerBaseComponents";

const localVue = registerBaseComponents();

describe("WalletAddressInput Component", () => {
  it("mounts", () => {
    const wrapper = shallowMount(WalletAddressInput, { localVue });
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(WalletAddressInput)).to.be.true;
  });

  it("mounts with right label", () => {
    const wrapper = mount(WalletAddressInput, { localVue });
    const rightLabel = wrapper.find(".input-group-append");
    expect(rightLabel.exists()).to.be.true;
    expect(rightLabel.find(".input-group-text").exists()).to.be.true;
  });

  it("emits an event when input is changed", () => {
    const wrapper = mount(WalletAddressInput, { localVue });
    const input = wrapper.find("input");
    const emitted = wrapper.emitted();
    input.setValue("1");
    expect(emitted["address-change"] && emitted["address-change"][0]).to.eql([
      "1"
    ]);
  });
});

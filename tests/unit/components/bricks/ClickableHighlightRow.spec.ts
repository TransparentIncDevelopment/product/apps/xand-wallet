import { shallowMount } from "@vue/test-utils";
import { expect } from "chai";

import ClickableHighlightRow from "../../../../src/components/bricks/ClickableHighlightRow.vue";

describe("Clickable Highlight Row", () => {
  it("mounts", () => {
    const wrapper = shallowMount(ClickableHighlightRow);
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(ClickableHighlightRow)).to.be.true;
  });

  it("mounts with a right button", () => {
    const wrapper = shallowMount(ClickableHighlightRow, {
      propsData: { showButton: true }
    });
    expect(wrapper.find(".material-icons").exists()).to.be.true;
  });

  it("Emits a click-item event when clicked", async () => {
    const wrapper = shallowMount(ClickableHighlightRow);
    const emitted = wrapper.emitted();
    wrapper.find(".label-container").trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted["click-item"] && emitted["click-item"].length).to.equal(1);
  });

  it("Emits a click-right-button event when button clicked", async () => {
    const wrapper = shallowMount(ClickableHighlightRow, {
      propsData: { showButton: true }
    });
    const emitted = wrapper.emitted();
    wrapper.find(".view-btn").trigger("click");
    await wrapper.vm.$nextTick();
    expect(
      emitted["click-right-button"] && emitted["click-right-button"].length
    ).to.equal(1);
  });
});

import { shallowMount } from "@vue/test-utils";
import { expect } from "chai";

import BaseModal from "../../../../src/components/base/BaseModal.vue";

describe("BaseModal Component", () => {
  it("mounts", () => {
    const wrapper = shallowMount(BaseModal);
    expect(wrapper.exists()).to.be.true;
    expect(wrapper.is(BaseModal)).to.be.true;
  });

  it("mounts with back button", () => {
    const wrapper = shallowMount(BaseModal, {
      propsData: {
        backEnabled: true
      }
    });
    const backBtn = wrapper.find(".xand-modal-back");
    expect(backBtn.exists()).to.be.true;
  });

  it("emits a back event when back is clicked", async () => {
    const wrapper = shallowMount(BaseModal, {
      propsData: {
        backEnabled: true
      }
    });
    const emitted = wrapper.emitted();
    const backBtn = wrapper.find(".xand-modal-back");
    expect(backBtn.exists()).to.be.true;
    backBtn.trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted.back && emitted.back[0]).to.eql([]);
  });

  it("emits a close event when close is clicked", async () => {
    const wrapper = shallowMount(BaseModal, {
      propsData: {
        backEnabled: true
      }
    });
    const emitted = wrapper.emitted();
    const closeBtn = wrapper.find(".xand-modal-close");
    expect(closeBtn.exists()).to.be.true;
    closeBtn.trigger("click");
    await wrapper.vm.$nextTick();
    expect(emitted.close && emitted.close[0]).to.eql([]);
  });
});

// tslint:disable: no-shadowed-variable
import { expect } from "chai";
import moment from "moment";
import {
  BankAccount,
  OperationType,
  TransactionState
} from "xand-member-api-client";

import enrich from "@/lib/enrichTransactionList";
import displayMessages from "@/constants/displayMessages";

describe("Enriching transaction history", () => {
  const memberAddress = "my address";

  const oceanBankAccount: BankAccount = {
    id: 123,
    bankName: "Ocean",
    shortName: "Some account name",
    bankId: 4,
    routingNumber: "444-444-444",
    maskedAccountNumber: "5412"
  };

  const baseOperation = {
    status: { state: TransactionState.Confirmed },
    datetime: "2020-05-04T20:37:24Z"
  };

  describe("When I enrich a $12.95 CreationRequest (id: 1234)", () => {
    const operation = {
      ...baseOperation,
      transactionId: "1234",
      operation: OperationType.CreationRequest,
      amountInMinorUnit: 1295,
      destinationAddress: "abc123"
    };

    const enrichedTransaction = enrich([operation], 1, null, memberAddress)[0];

    it("to/from address is 'Bank to Wallet'", () => {
      expect(enrichedTransaction.toOrFromAddress).to.equal("Bank to Wallet");
    });

    it("AmountDeducted is false", () => {
      expect(enrichedTransaction.isAmountDeducted).to.be.false;
    });

    it("value is 1295", () => {
      expect(enrichedTransaction.value).to.equal(1295);
    });

    it("valueAsCurrency is $12.95", () => {
      expect(enrichedTransaction.valueAsCurrency).to.equal("$12.95");
    });

    it("formatted datetime is correct for local timezone", () => {
      expect(enrichedTransaction.formattedDatetime).to.equal(
        moment(enrichedTransaction.datetime).format(
          displayMessages.LOCALIZED_DATETIME_FORMAT
        )
      );
    });

    it("transaction type for UI is 'Transfer to Wallet'", () => {
      expect(enrichedTransaction.txnTypeLabel).to.equal("Transfer to Wallet");
    });

    it("id is 1234", () => {
      expect(enrichedTransaction.txnId).to.equal("1234");
    });

    it("hasReceipt is false", () => {
      expect(enrichedTransaction.hasReceipt).to.be.false;
    });

    describe("With bank 'Ocean' and account '5412'", () => {
      const enrichedWithBankInfo = enrich(
        [{ ...operation, bankAccount: oceanBankAccount }],
        1,
        null,
        memberAddress
      )[0];

      it("to/from address is 'Ocean: x5412 to Wallet'", () => {
        expect(enrichedWithBankInfo.toOrFromAddress).to.equal(
          "Ocean: x5412 to Wallet"
        );
      });
    });
  });

  describe("When I enrich a Redemption (id: 5555) for $98.76", () => {
    const operation = {
      ...baseOperation,
      transactionId: "5555",
      operation: OperationType.Redeem,
      amountInMinorUnit: 9876,
      destinationAddress: "abc123"
    };
    const enrichedTransaction = enrich([operation], 1, null, memberAddress)[0];

    describe("With bank 'Ocean' and account '5412'", () => {
      const enrichedWithBankInfo = enrich(
        [{ ...operation, bankAccount: oceanBankAccount }],
        1,
        null,
        memberAddress
      )[0];
      it("to/from address is 'Wallet to Ocean: x5412'", () => {
        expect(enrichedWithBankInfo.toOrFromAddress).to.equal(
          "Wallet to Ocean: x5412"
        );
      });
    });

    it("to/from address is 'Wallet to Bank'", () => {
      expect(enrichedTransaction.toOrFromAddress).to.equal("Wallet to Bank");
    });

    it("AmountDeducted is true", () => {
      expect(enrichedTransaction.isAmountDeducted).to.be.true;
    });

    it("value is 9876", () => {
      expect(enrichedTransaction.value).to.equal(9876);
    });

    it("valueAsCurrency is $98.76", () => {
      expect(enrichedTransaction.valueAsCurrency).to.equal("$98.76");
    });

    it("formatted datetime is correct for local timezone", () => {
      expect(enrichedTransaction.formattedDatetime).to.equal(
        moment(enrichedTransaction.datetime).format(
          displayMessages.LOCALIZED_DATETIME_FORMAT
        )
      );
      it("id is 5555", () => {
        expect(enrichedTransaction.txnId).to.equal("5555");
      });
    });
  });

  const contact = { william: { contactName: "Bob", company: "Blah, Inc." } };

  describe("Given a Payment (id: 7777) from me to Bob(Blah inc.) for $25.89", () => {
    const operation = {
      ...baseOperation,
      transactionId: "7777",
      operation: OperationType.Payment,
      amountInMinorUnit: 2589,
      destinationAddress: "william",
      signerAddress: memberAddress
    };

    describe("When I enrich it", () => {
      const enrichedTransaction = enrich(
        [operation],
        1,
        contact,
        memberAddress
      )[0];

      it("has receipt", () => {
        expect(enrichedTransaction.hasReceipt).to.be.true;
      });

      it("has value 2589", () => {
        expect(enrichedTransaction.value).to.equal(2589);
      });

      it("valueAsCurrency is $25.89", () => {
        expect(enrichedTransaction.valueAsCurrency).to.equal("$25.89");
      });

      it("formatted datetime is correct for local timezone", () => {
        expect(enrichedTransaction.formattedDatetime).to.equal(
          moment(enrichedTransaction.datetime).format(
            displayMessages.LOCALIZED_DATETIME_FORMAT
          )
        );
      });

      it("id is 7777", () => {
        expect(enrichedTransaction.txnId).to.equal("7777");
      });

      it("isAmountDeducted is true", () => {
        expect(enrichedTransaction.isAmountDeducted).to.be.true;
      });

      it("transaction type for UI is 'Payment Sent'", () => {
        expect(enrichedTransaction.txnTypeLabel).to.equal("Payment Sent");
      });

      it("to/from contact is 'Bob - Blah, Inc.'", () => {
        expect(enrichedTransaction.toOrFromContact).to.equal(
          "Bob - Blah, Inc."
        );
      });

      it("to/from address is Bob's address", () => {
        expect(enrichedTransaction.toOrFromAddress).to.equal("william");
      });
    });
  });

  describe("Given a Payment (id: 8888) from Bob(Blah inc.) to me for $13,281,321.78", () => {
    const operation = {
      ...baseOperation,
      transactionId: "8888",
      operation: OperationType.Payment,
      amountInMinorUnit: 1328132178,
      destinationAddress: memberAddress,
      signerAddress: "william"
    };

    describe("When I enrich it", () => {
      const enrichedTransaction = enrich(
        [operation],
        1,
        contact,
        memberAddress
      )[0];

      it("has receipt", () => {
        expect(enrichedTransaction.hasReceipt).to.be.true;
      });

      it("has value 1328132178", () => {
        expect(enrichedTransaction.value).to.equal(1328132178);
      });

      it("valueAsCurrency is $13,281,321.78", () => {
        expect(enrichedTransaction.valueAsCurrency).to.equal("$13,281,321.78");
      });

      it("formatted datetime is correct for local timezone", () => {
        expect(enrichedTransaction.formattedDatetime).to.equal(
          moment(enrichedTransaction.datetime).format(
            displayMessages.LOCALIZED_DATETIME_FORMAT
          )
        );
      });

      it("id is 8888", () => {
        expect(enrichedTransaction.txnId).to.equal("8888");
      });

      it("isAmountDeducted is false", () => {
        expect(enrichedTransaction.isAmountDeducted).to.be.false;
      });

      it("transaction type for UI is 'Payment Received'", () => {
        expect(enrichedTransaction.txnTypeLabel).to.equal("Payment Received");
      });

      it("to/from contact is 'Bob - Blah, Inc.'", () => {
        expect(enrichedTransaction.toOrFromContact).to.equal(
          "Bob - Blah, Inc."
        );
      });

      it("to/from address is Bob's address", () => {
        expect(enrichedTransaction.toOrFromAddress).to.equal("william");
      });
    });
  });

  describe("When I enrich with operation with count = 4", () => {
    const transactionCount = 4;
    const operation = {
      transactionId: "1234",
      operation: OperationType.CreationRequest,
      amountInMinorUnit: 12.95,
      status: { state: TransactionState.Confirmed },
      destinationAddress: "abc123",
      datetime: "2020-05-04T20:37:24Z"
    };

    const enrichedTransaction = enrich(
      [operation],
      transactionCount,
      null,
      memberAddress
    )[0];

    it("enriched transaction has a total count of 4", () => {
      expect(enrichedTransaction.latestTotalTxns).to.equal(4);
    });
  });

  describe("When I enrich a pending redeem/create", () => {
    const operation = {
      transactionId: "1234",
      operation: OperationType.CreationRequest,
      amountInMinorUnit: 12.95,
      status: { state: TransactionState.Pending },
      destinationAddress: "abc123",
      datetime: "2020-05-04T20:37:24Z"
    };

    const enrichedTransaction = enrich([operation], 1, null, memberAddress)[0];

    it("enriched transaction has a pending label", () => {
      expect(enrichedTransaction.txnTypeLabel).to.equal("Transfer Pending");
    });
  });

  describe("When I enrich a cancelled redeem/create", () => {
    const operation = {
      transactionId: "1234",
      operation: OperationType.CreationRequest,
      amountInMinorUnit: 12.95,
      status: { state: TransactionState.Cancelled },
      destinationAddress: "abc123",
      datetime: "2020-05-04T20:37:24Z"
    };

    const enrichedTransaction = enrich([operation], 1, null, memberAddress)[0];

    it("enriched transaction has a cancelled label", () => {
      expect(enrichedTransaction.txnTypeLabel).to.equal("Transfer Cancelled");
    });
  });
});

import { expect } from "chai";
import MemberApiConnection from "@/lib/MemberApiConnection";

describe("Given", () => {
  let memberApiConnection: MemberApiConnection;

  describe("an instance of MemberApiConnection with a jwt token and trailing slash api url", () => {
    before(() => {
      memberApiConnection = new MemberApiConnection(
        "https://www.google.com/",
        "token!!!"
      );
    });

    it("returns the correctly formatted Member API path", () => {
      expect(memberApiConnection.getMemberApiUrl()).to.equal(
        "https://www.google.com/api/v1"
      );
    });

    it("returns the correctly formatted jwt token auth header", () => {
      expect(memberApiConnection.getAuthHeader()).to.eql({
        Authorization: "Bearer token!!!"
      });
    });
  });

  describe("an instance of MemberApiConnection with no token and no trailing slash for the api url", () => {
    before(() => {
      memberApiConnection = new MemberApiConnection("https://www.google.com");
    });

    it("returns the correctly formatted Member API path", () => {
      expect(memberApiConnection.getMemberApiUrl()).to.equal(
        "https://www.google.com/api/v1"
      );
    });

    it("returns an empty object instead of an auth header", () => {
      expect(memberApiConnection.getAuthHeader()).to.be.empty;
    });
  });
});

import fs from "fs";
import File from "@/lib/File/File";
import { expect } from "chai";

describe("Given an instance of filestream", () => {
  const fileName = "fredbob.test-output";

  before(() => {
    if (fs.existsSync(fileName)) {
      fs.unlinkSync(fileName);
    }
  });

  it("can be written to", async () => {
    const fileStream = new File(fileName);
    const text = "This is a test of the emergency broadcast system";
    await fileStream.openWrite(writer => {
      writer.writeLine(text);
    });

    expect(fileStream.exists()).to.equal(true);
    expect(fileStream.size().bytes()).to.equal(text.length + 1);
  });

  it("can be deleted", async () => {
    const fileStream = new File(fileName);
    const text = "This is a test of the emergency broadcast system";
    await fileStream.openWrite(writer => {
      writer.writeLine(text);
    });

    fileStream.delete();

    expect(fileStream.exists()).to.equal(false);
  });
});

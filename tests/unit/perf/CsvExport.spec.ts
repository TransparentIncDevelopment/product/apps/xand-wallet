import {
  OperationType,
  Transaction,
  TransactionHistory,
  TransactionState
} from "xand-member-api-client/dist";
import fs from "fs";
import { expect } from "chai";
import CsvExporter from "@/lib/Csv/CsvExporter";
import File from "@/lib/File/File";
import { IMemberApiService } from "@/services/IMemberApiService";
import td from "testdouble";

describe("Given a large data set", () => {
  //
  // timeout value here is somewhat constrained by what
  // the build server is capable of.
  //
  const MAX_TIMEOUT_VALUE = 60_000;

  let records: Transaction[];
  let history: TransactionHistory;

  const userDetails = {
    registered: true,
    userBalanceInMinorUnit: 123.45,
    lastRefreshedDateTime: "dsaf",
    walletBalanceStatus: "some status",
    address: "my address"
  };

  const baseOperation = {
    status: { state: TransactionState.Confirmed },
    datetime: "2020-02-05T19:00:00Z"
  };

  function newTransaction(): Transaction {
    return {
      ...baseOperation,
      bankAccount: {
        shortName: "Joe Exotic's account",
        id: 1,
        bankId: 10,
        bankName: "Joe Exotic's Tiger Bank",
        routingNumber: "666",
        maskedAccountNumber: "9876"
      },
      transactionId: "1234",
      operation: OperationType.CreationRequest,
      amountInMinorUnit: 1295,
      destinationAddress: "abc123"
    };
  }

  before(() => {
    records = [];

    /*
        125000 is not an arbitrary number. It was chosen
        because this is the maximum number of records we expect to support
        in Xand API.

        We will come back later and implement a paged solution to this problem
        so that a virtually infinte number of records are storable.
    */

    for (let i = 0; i < 125000; i++) {
      records.push(newTransaction());
    }

    history = {
      transactions: records,
      total: records.length
    };
  });

  describe("And I use the CSV Exporter class", () => {
    const fileName = "test-output.csv";

    before(() => {
      if (fs.existsSync(fileName)) {
        fs.unlinkSync(fileName);
      }
    });

    it("Then it generates the csv file in a reasonable time.", async function() {
      this.timeout(MAX_TIMEOUT_VALUE);

      const svc = td.object<IMemberApiService>();
      td.when(
        svc.getHistory({
          pageSize: 125000,
          pageNumber: 1
        })
      ).thenResolve(await history);

      const fileStream = new File(fileName);
      const exporter = new CsvExporter(fileStream, userDetails.address, svc);
      await exporter.export();

      expect(fileStream.exists()).to.equal(true);
      expect(fileStream.size().kilobytes()).to.be.greaterThan(15);
    });
  });
});

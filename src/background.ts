"use strict";

import { app, BrowserWindow, Menu, globalShortcut, protocol } from "electron";
import fs from "fs";
import path from "path";
import os from "os";
import {
  createProtocol,
  installVueDevtools
} from "vue-cli-plugin-electron-builder/lib";
import winston from "winston";
import isDev from "electron-is-dev";

const windowWidth = 1000;
const windowHeight = 850;

declare const __static: string;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: BrowserWindow | null;

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: "app", privileges: { secure: false, standard: true } }
]);

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: windowWidth,
    height: windowHeight,
    resizable: false,
    icon: path.join(__static, "icon.png"),
    webPreferences: {
      nodeIntegration: true,
      webSecurity: false
    }
  });

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    win.loadURL(process.env.WEBPACK_DEV_SERVER_URL);
    if (!process.env.IS_TEST) {
      win.webContents.openDevTools();
    }
  } else {
    createProtocol("app");
    // Load the index.html when not in development
    win.loadURL("app://./index.html");
  }

  win.on("closed", () => {
    win = null;
  });
}

/* Default Functionality Override */
function shortcutAccess() {
  globalShortcut.register("Alt+E", () => {
    globalShortcut.unregisterAll();
    app.quit();
  });
  globalShortcut.register("Alt+R", () => {
    if (win) {
      if (!win.isResizable()) {
        win.setResizable(true);
      } else {
        win.setSize(windowWidth, windowHeight);
        win.setResizable(false);
      }
    }
  });
  globalShortcut.register("Alt+I", () => {
    if (win) {
      win.webContents.toggleDevTools();
    }
  });
}

/* Userkey and Winston Logger Configuration */
const PATH = {
  DATA: path.join(app.getPath("userData"), "Data"),
  USER: path.join(app.getPath("userData"), "Data", "User"),
  HISTORY: path.join(app.getPath("userData"), "Data", "User", "History"),
  LOG: path.join(app.getPath("userData"), "Data", "Log")
};

function userdataPath() {
  (() => fs.existsSync(PATH.DATA) || fs.mkdirSync(PATH.DATA))();
  (() => fs.existsSync(PATH.USER) || fs.mkdirSync(PATH.USER))();
  (() => fs.existsSync(PATH.HISTORY) || fs.mkdirSync(PATH.HISTORY))();
  (() => fs.existsSync(PATH.LOG) || fs.mkdirSync(PATH.LOG))();
}

const globalAny: any = global;

/* Initializing Winston Logger */
function loggerInit() {
  globalAny.logger = {
    error: 0,
    warn: 1,
    info: 2,
    verbose: 3,
    debug: 4,
    silly: 5
  };

  for (const level of Object.keys(globalAny.logger)) {
    const logger: any = winston.createLogger({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.prettyPrint()
      ),
      transports: [
        new winston.transports.Console({
          level
        }),
        new winston.transports.File({
          dirname: PATH.LOG,
          filename: `_${level}.log`,
          level,
          handleExceptions: false,
          maxFiles: 2,
          maxsize: 5242880 // 5MB
        })
      ]
    });
    globalAny.logger[level] = logger[level];
  }
}

/* Work around to enable copy paste in MAC without displaying menubar in window */
function createMenu() {
  const application = {
    label: "Application",
    submenu: [
      {
        label: "Quit",
        accelerator: "CommandOrControl+Q",
        click: () => {
          app.quit();
        }
      }
    ]
  };

  // TODO: Remove this, test on mac.
  const edit = {
    label: "Edit",
    submenu: [
      {
        label: "Undo",
        accelerator: "CmdOrCtrl+Z",
        selector: "undo:"
      },
      {
        label: "Redo",
        accelerator: "Shift+CmdOrCtrl+Z",
        selector: "redo:"
      },
      {
        type: "separator"
      },
      {
        label: "Cut",
        accelerator: "CmdOrCtrl+X",
        selector: "cut:"
      },
      {
        label: "Copy",
        accelerator: "CmdOrCtrl+C",
        selector: "copy:"
      },
      {
        label: "Paste",
        accelerator: "CmdOrCtrl+V",
        selector: "paste:"
      },
      {
        label: "Select All",
        accelerator: "CmdOrCtrl+A",
        selector: "selectAll:"
      }
    ]
  };

  const template: any = [application, edit];

  globalAny.osType = os.type();
  if (os.type() === "Darwin") {
    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
  } else {
    Menu.setApplicationMenu(null);
  }
}

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
    shortcutAccess();
    createMenu();
    userdataPath();
    loggerInit();
  }
});

function shouldInstallVueDevTools() {
  return isDev && !process.env.IS_TEST;
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", async () => {
  if (shouldInstallVueDevTools()) {
    try {
      await installVueDevtools();
    } catch (e) {
      globalAny.logger.error("Vue Devtools failed to install:", e.toString());
    }
  }
  createWindow();
  shortcutAccess();
  createMenu();
  userdataPath();
  loggerInit();
});

// Exit cleanly on request from parent process in development mode.
if (isDev) {
  if (process.platform === "win32") {
    process.on("message", (data: string) => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}

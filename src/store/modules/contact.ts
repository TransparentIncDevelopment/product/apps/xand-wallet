import fs from "fs";
import { join } from "path";
import Vue from "vue";
import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

import appSettings from "@/constants/appSettings";
import __env from "@/env";
import Contact from "@/models/Contact";
import { loggerIndex, loggerMessage } from "@/constants/logger";

interface ErrorLogger {
  error: (s: string) => void;
}

function makeContactForAddress(userkey: any): string {
  return (appSettings.CONTACT_FILE_NAME + "-" + userkey) as string;
}

@Module({ namespaced: false, name: "contacts" })
export default class ContactStore extends VuexModule {
  public Contacts: any = {};

  public get contacts(): any {
    return this.Contacts;
  }

  @Mutation
  public setContacts(contacts: any): void {
    this.Contacts = contacts;
  }

  @Mutation
  public upsertContactState(contact: Contact): void {
    const { address, contactName, company } = contact;
    Vue.set(this.Contacts, address, { contactName, company });
  }

  @Mutation
  public deleteContactState(address: string): void {
    Vue.delete(this.Contacts, address);
  }

  @Action({ rawError: true })
  public async fetchContactsData(logger?: ErrorLogger) {
    let contactsData: any;
    const contactsFileName = makeContactForAddress(
      this.context.rootGetters.address
    );
    const contactsFilePath = join(__env.certStoragePath, contactsFileName);
    if (fs.existsSync(contactsFilePath)) {
      try {
        contactsData = JSON.parse(
          (await fs.promises.readFile(contactsFilePath, "utf-8")) as string
        );
      } catch (err) {
        if (logger) {
          logger.error(loggerIndex.ERR_.SER0046 + err);
          logger.error(loggerMessage.CONTACT_FILE_READING_FAILED + err);
        }
      }
    } else {
      contactsData = {};
    }
    this.context.commit("setContacts", contactsData);
  }

  @Action({ rawError: true })
  public async upsertContact(payload: {
    contact: Contact;
    logger?: ErrorLogger;
  }) {
    const { contact, logger } = payload;
    try {
      this.context.commit("upsertContactState", contact);

      if (!contact.address && logger) {
        logger.error(loggerMessage.CONTACT_INVALID_ADDRESS);
      }
      if (!contact.company && logger) {
        logger.error(loggerMessage.CONTACT_MISSING_CO);
      }
      await this.context.dispatch("saveContactsDataToFile", logger);
    } catch (err) {
      if (logger) {
        logger.error(loggerMessage.CONTACT_STATE_UPSERT_FAILED);
      }
    }
  }

  @Action({ rawError: true })
  public async deleteContact(payload: {
    address: string;
    logger?: ErrorLogger;
  }) {
    const { address, logger } = payload;
    try {
      this.context.commit("deleteContactState", address);
      await this.context.dispatch("saveContactsDataToFile", logger);
    } catch (err) {
      if (logger) {
        logger.error(loggerMessage.CONTACT_STATE_DELETE_FAILED);
      }
    }
  }

  @Action({ rawError: true })
  public async saveContactsDataToFile(logger?: ErrorLogger) {
    const contactsFileName = makeContactForAddress(
      this.context.rootGetters.address
    );
    const contactsFilePath = join(__env.certStoragePath, contactsFileName);
    try {
      await fs.promises.writeFile(
        contactsFilePath,
        JSON.stringify(this.context.rootGetters.contacts)
      );
    } catch (err) {
      if (logger) {
        logger.error(loggerIndex.ERR_.SER0047 + err);
        logger.error(loggerMessage.CONTACT_FILE_WRITING_FAILED);
      }
    }
  }
}

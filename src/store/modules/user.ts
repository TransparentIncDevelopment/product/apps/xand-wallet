import { Inject } from "inversify-props";
import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";

import appSettings from "@/constants/appSettings";
import UserDetails from "@/models/UserDetails";
import TransactionToaster from "@/lib/TransactionToaster";
import { IMemberApiService } from "@/services/IMemberApiService";
import displayMessages from "@/constants/displayMessages";

class XandInjection {
  @Inject("IMemberApiService")
  public api!: IMemberApiService;
}

// tslint:disable-next-line: max-classes-per-file
@Module({ namespaced: false, name: "user" })
export default class UserStore extends VuexModule {
  private userDetailsState: UserDetails = {
    address: "",
    registered: false,
    userBalanceInMinorUnit: 0,
    lastRefreshedDateTime: ""
  };

  private refreshHistoryPollerState: NodeJS.Timeout | undefined = undefined;

  public get userDetails(): UserDetails {
    return this.userDetailsState;
  }

  public get refreshHistoryPoller() {
    return this.refreshHistoryPollerState;
  }

  @Action({ rawError: true })
  public async refreshUserDetails(toaster: TransactionToaster): Promise<void> {
    try {
      const injections = new XandInjection();
      const address = (await injections.api.getMember()).address;
      const walletBalance = await injections.api.getXANDBalance();
      const userDetails: UserDetails = {
        address,
        userBalanceInMinorUnit: walletBalance.balanceInMinorUnit,
        lastRefreshedDateTime: new Date(),
        registered: true
      };
      await this.context.dispatch("setUserDetails", {
        details: userDetails,
        toaster
      });
    } catch (e) {
      await this.context.dispatch("setUserDetails", {
        details: {
          walletBalanceStatus: displayMessages.BALANCE_FETCHING_STUCK
        },
        toaster
      });
      throw e;
    }
  }

  @Mutation
  public setUserDetailsMutation(payload: {
    details: UserDetails;
    toaster: TransactionToaster;
  }) {
    const { details, toaster } = payload;
    if (
      this.userDetailsState &&
      details &&
      details.address &&
      this.userDetailsState.address !== details.address
    ) {
      toaster.setReady(false);
    }
    this.userDetailsState = Object.assign(this.userDetailsState, details);
  }

  @Mutation
  public setRefreshHistoryPoller(timeout: NodeJS.Timeout | undefined) {
    this.refreshHistoryPollerState = timeout;
  }

  @Action({ rawError: true })
  public setUserDetails(payload: {
    details: UserDetails;
    toaster: TransactionToaster;
  }) {
    this.context.commit("setUserDetailsMutation", payload);
  }

  @Action({ rawError: true })
  public stopRefreshPoller() {
    if (this.context.getters.refreshHistoryPoller) {
      clearInterval(this.context.getters.refreshHistoryPoller);
      this.context.commit("setRefreshHistoryPoller", undefined);
    }
  }

  @Action({ rawError: true })
  public attemptStartRefreshPoller(toaster: TransactionToaster) {
    if (
      this.context.getters.userDetails.address &&
      !this.context.getters.refreshHistoryPoller
    ) {
      const refreshHistoryPoller = setInterval(() => {
        this.context.dispatch("refreshWalletBalance", toaster);
        this.context.dispatch("getTransactionHistoryPage", {
          toaster
        });
      }, appSettings.POLLING_REFRESH_TRANSACTION_HISTORY_INTERVAL_MS);
      this.context.commit("setRefreshHistoryPoller", refreshHistoryPoller);
    } else if (!this.context.getters.userDetails.address) {
      this.context.dispatch("stopRefreshPoller");
    }
  }
}

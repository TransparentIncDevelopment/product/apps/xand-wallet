import { Action, Module, Mutation, VuexModule } from "vuex-module-decorators";
import { Inject } from "inversify-props";
import { IUserSettingsStorage } from "@/persistence/interfaces";
import { IMemberApiService } from "@/services/IMemberApiService";
import __env from "@/env";
import TransactionToaster from "@/lib/TransactionToaster";

class SettingsInjection {
  @Inject("IUserSettingsStorage")
  public userSettingsStorage!: IUserSettingsStorage;

  @Inject("IMemberApiService")
  public api!: IMemberApiService;
}

// tslint:disable-next-line: max-classes-per-file
@Module({ namespaced: false, name: "settings" })
export default class SettingsStore extends VuexModule {
  public currentJwtToken: string | undefined = undefined;
  public currentMemberApiUrl: string = "";

  get jwtToken() {
    return this.currentJwtToken;
  }

  get memberApiUrl() {
    return this.currentMemberApiUrl;
  }

  @Mutation
  public setJwtTokenMutation(token: string | undefined) {
    this.currentJwtToken = token;
    const injections = new SettingsInjection();
    injections.userSettingsStorage.setJwtToken(token);
    injections.api.updateJwt(token);
  }

  @Mutation
  public setMemberApiUrlMutation(url: string) {
    this.currentMemberApiUrl = url;
    const injections = new SettingsInjection();
    injections.userSettingsStorage.setMemberApiUrl(url);
    injections.api.updateUrl(url);
  }

  @Action({ rawError: true })
  public setJwtToken(token: string | undefined) {
    this.context.commit("setJwtTokenMutation", token);
  }

  @Action({ rawError: true })
  public setMemberApiUrl(url: string) {
    this.context.commit("setMemberApiUrlMutation", url);
  }

  @Action({ rawError: true })
  public settingsChangeRefresh(toaster: TransactionToaster) {
    this.context.dispatch("clearAllAccounts");
    this.context.dispatch("refreshAllAccounts");
    this.context.dispatch("refreshAllBanks");
    this.context.dispatch("loadBankAccountId");
    this.context.dispatch("getTransactionHistoryPage", { toaster });
    this.context.dispatch("refreshWalletBalance", toaster);
  }

  @Action({ rawError: true })
  public loadJwtToken() {
    const injections = new SettingsInjection();
    const currentJwt = injections.userSettingsStorage.getJwtToken();
    this.context.commit("setJwtTokenMutation", currentJwt);
  }

  @Action({ rawError: true })
  public loadMemberApiUrl() {
    const injections = new SettingsInjection();
    const currentUrl = injections.userSettingsStorage.getMemberApiUrl();
    this.context.commit(
      "setMemberApiUrlMutation",
      currentUrl ? currentUrl : __env.MEMBER_API_URL
    );
  }
}

export interface IUserSettingsStorage {
  getMRUBankAccount: (address: string) => number | undefined;
  setMRUBankAccount: (
    address: string,
    bankAccountId: number | undefined
  ) => void;

  getMemberApiUrl: () => string | undefined;
  setMemberApiUrl: (url: string | undefined) => void;

  getJwtToken: () => string | undefined;
  setJwtToken: (token: string | undefined) => void;
}

import { inject } from "inversify-props";
import { IUserSettingsStorage } from "@/persistence/interfaces";

export default class UserSettingsStorage implements IUserSettingsStorage {
  private static getLocalStorageKeyName(itemName: string): string {
    return `user-settings-${itemName}`;
  }

  constructor(@inject("Storage") private storage: Storage) {}

  public getMemberApiUrl(): string | undefined {
    const keyName = UserSettingsStorage.getLocalStorageKeyName(
      "member-api-url"
    );
    const returnValue = this.storage.getItem(keyName);
    return returnValue === null ? undefined : returnValue;
  }

  public setMemberApiUrl(url: string | undefined): void {
    const keyName = UserSettingsStorage.getLocalStorageKeyName(
      "member-api-url"
    );
    if (url) {
      this.storage.setItem(keyName, url);
    } else {
      this.storage.removeItem(keyName);
    }
  }

  public getJwtToken(): string | undefined {
    const keyName = UserSettingsStorage.getLocalStorageKeyName("jwt");
    const returnValue = this.storage.getItem(keyName);
    return returnValue === null ? undefined : returnValue;
  }

  public setJwtToken(token: string | undefined): void {
    const keyName = UserSettingsStorage.getLocalStorageKeyName("jwt");
    if (token) {
      this.storage.setItem(keyName, token);
    } else {
      this.storage.removeItem(keyName);
    }
  }

  public getMRUBankAccount(address: string): number | undefined {
    const keyName = UserSettingsStorage.getLocalStorageKeyName(
      `bank-account-name-${address}`
    );
    const strValue = this.storage.getItem(keyName);
    if (strValue === null) {
      return undefined;
    }

    const returnValue = parseInt(strValue, 10);
    return isNaN(returnValue) ? undefined : returnValue;
  }

  public setMRUBankAccount(
    address: string,
    bankAccountId: number | undefined
  ): void {
    const keyName = UserSettingsStorage.getLocalStorageKeyName(
      `bank-account-name-${address}`
    );
    if (bankAccountId === undefined) {
      this.storage.removeItem(keyName);
    } else {
      this.storage.setItem(keyName, String(bankAccountId));
    }
  }
}

import _Vue from "vue";
import Money from "tpfs-money";

import BigNumber from "bignumber.js";

const plugin = {
  install(Vue: typeof _Vue) {
    Vue.filter(
      "minor-unit-currency",
      (val: number | string | BigNumber | undefined) => {
        if (val === undefined) {
          return "";
        }
        return Money.displayCurrency("en-US", "USD", val);
      }
    );
  }
};

export default plugin;

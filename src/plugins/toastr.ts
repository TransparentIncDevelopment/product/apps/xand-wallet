import * as toastr from "toastr";
import router from "@/router";
import TransactionToaster from "@/lib/TransactionToaster";
import EnrichedTransaction from "@/models/EnrichedTransaction";
import displayMessages from "@/constants/displayMessages";

const txToaster = new TransactionToaster(false, (txn: EnrichedTransaction) => {
  if (txn.txnTypeLabel === displayMessages.txHistory.paymentReceived) {
    // Open received receipt
    router.replace({
      name: `receipt`,
      params: {
        txid: txn.txnId,
        senderKey: txn.toOrFromAddress,
        amount: txn.value.toString()
      }
    });
  } else if (txn.txnTypeLabel === displayMessages.txHistory.paymentSent) {
    // Open sent receipt
    router.replace({
      name: `receipt`,
      params: {
        txid: txn.txnId,
        receiverKey: txn.toOrFromAddress,
        amount: txn.value.toString()
      }
    });
  }
});

export default {
  install(Vue: any) {
    // add the instance method
    if (!Vue.prototype.hasOwnProperty("$toast")) {
      Object.defineProperty(Vue.prototype, "$toast", {
        get: function get() {
          return toastr;
        }
      });
    }
    // add the instance method for the transaction toaster
    if (!Vue.prototype.hasOwnProperty("$txToast")) {
      Object.defineProperty(Vue.prototype, "$txToast", {
        get: function get() {
          return txToaster;
        }
      });
    }
  }
};

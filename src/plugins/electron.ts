import electron from "electron";
import _Vue from "vue";

const $remoteLogger = electron.remote.getGlobal("logger");
const $remoteDialog = electron.remote.dialog;
const $getLoggerError = $remoteLogger.error;

const plugin = {
  install(Vue: typeof _Vue) {
    Vue.prototype.$remoteLogger = $remoteLogger;
    Vue.prototype.$remoteDialog = $remoteDialog;
    Vue.prototype.$getLoggerError = $getLoggerError;
  }
};
export { plugin as default, $remoteDialog, $remoteLogger, $getLoggerError };

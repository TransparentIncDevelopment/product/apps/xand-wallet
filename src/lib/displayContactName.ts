import Contact from "@/models/Contact";

export default function displayContactName(contact: Contact) {
  return `${contact.contactName ? contact.contactName + " - " : ""}${
    contact.company
  }`;
}

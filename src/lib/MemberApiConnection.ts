import urljoin from "url-join";

const MEMBER_API_PATH = "api/v1";

export interface Headers {
  Authorization?: string;
}

export default class MemberApiConnection {
  public baseUrl: string;
  public jwtToken: string | undefined;

  constructor(baseUrl: string, jwtToken?: string) {
    this.baseUrl = baseUrl;
    this.jwtToken = jwtToken;
  }

  public getMemberApiUrl(): string {
    return urljoin(this.baseUrl, MEMBER_API_PATH);
  }

  public getAuthHeader(): Headers {
    if (this.jwtToken) {
      return { Authorization: `Bearer ${this.jwtToken}` };
    } else {
      return {};
    }
  }
}

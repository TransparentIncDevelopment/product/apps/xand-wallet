import appSettings from "@/constants/appSettings";
import displayMessages from "@/constants/displayMessages";

function pollForCondition(
  pollFn: () => Promise<void>,
  condition: () => boolean,
  maxAttempts: number = appSettings.MAX_RETRIES
) {
  return new Promise(async (resolve, reject) => {
    let retryCounter = 0;
    let poller = setTimeout(pollTask, appSettings.POLLING_INTERVAL_MS);

    async function pollTask() {
      if (++retryCounter > maxAttempts) {
        return stop(false, displayMessages.POLL_FAIL_MAX_RETRY);
      }
      try {
        await pollFn();
        if (condition()) {
          return stop(true, null);
        }
      } catch (err) {
        return stop(false, err);
      }
      poller = setTimeout(pollTask, appSettings.POLLING_INTERVAL_MS);
    }

    function stop(conditionMet: boolean, err: any | undefined) {
      clearTimeout(poller);
      conditionMet ? resolve() : reject(err);
    }
  });
}

export default pollForCondition;

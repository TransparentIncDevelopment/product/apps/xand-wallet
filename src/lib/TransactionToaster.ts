import * as toastr from "toastr";
import "toastr/build/toastr.min.css";
import { OperationType } from "xand-member-api-client";
import Money from "tpfs-money";

import EnrichedTransaction from "@/models/EnrichedTransaction";

export default class TransactionToaster {
  private oldTxnTotal = 0;

  /**
   * Initializes TransactionToaster with readiness state. It is the wallet's duty to perform an initial load
   * of recent transactions, and *then* tell the Toaster with `setReady(true)` that it is allowed to give
   * notifications for what it infers to be previously unseen Transactions.
   * @param hasEstablishedReadiness - Bool to tell Toaster when the wallet is ready to give
   *     notifications for "new" TXs
   * @param onNavigate - Callback for navigating to toast
   */
  constructor(
    private hasEstablishedReadiness: boolean,
    private onNavigate: (tx: any) => void
  ) {}

  /**
   * Set when initial transaction state has been loaded, *after* which, TransactionToaster will
   * begin giving notifications
   * @param isReady
   */
  public setReady(isReady: boolean) {
    this.hasEstablishedReadiness = isReady;
  }

  /**
   * This iterates through a transaction list and gives a notification if appropriate.
   * TODO: Currently, notifications for "new" transacations will only appear if the wallet
   * is querying for transaction history. Update this to properly receive updates
   * over some stream
   */
  public checkAndNotify(txList: EnrichedTransaction[]): void {
    if (!txList.length) {
      return;
    }
    // Transactions are passed sorted by most recent first. Reverse so that earlier TXs get toasted first.
    const txListRev = txList.reverse();
    const newTxnTotal = txList[0].latestTotalTxns;
    const diff = newTxnTotal - this.oldTxnTotal;
    const maxToastCount = Math.min(diff, 4);

    // Pop toasts for only the latest txns
    const txListToShow = txListRev.slice(0, maxToastCount);
    if (diff > 0) {
      // If ready and total of txns in history is greater than we have seen, notify
      txListToShow
        .filter(this.checkShouldNotify.bind(this))
        .forEach(this.notifyTransaction.bind(this));
    }

    // Save the latest total txn count seen so we can infer if we need to notify for future transactions
    this.oldTxnTotal = newTxnTotal;
  }

  private checkShouldNotify(tx: EnrichedTransaction): boolean {
    // Right now, only toasting for receive transactions. Potentially notify for other types in the future.
    return (
      this.hasEstablishedReadiness &&
      tx.operation === OperationType.Payment &&
      tx.isAmountDeducted === false
    );
  }

  private notifyTransaction(tx: EnrichedTransaction): void {
    const from =
      tx.toOrFromContact !== "" ? tx.toOrFromContact : tx.toOrFromAddress;
    // Format to cleanly fit in 1 line of the notification
    const fromFormatted = this.formatFromText(from);
    const toastOptions: any = {
      closeButton: true,
      progressBar: true,
      newestOnTop: true,
      showDuration: 300, // how long it takes for the toast to fade in
      hideDuration: 100, // how long it takes to fade out
      timeOut: 5000, // how long the notification lasts
      extendedTimeOut: 2000, // after user hovers over, how long until toast fades out
      positionClass: "toast-top-right"
    };
    if (this.onNavigate) {
      toastOptions.onclick = () => {
        this.onNavigate(tx);
      };
    }

    const valueMessage =
      tx.value && typeof tx.value === "number"
        ? Money.displayCurrency("en-US", "USD", tx.value)
        : `$${tx.value}`;
    toastr.success(
      `From: ${fromFormatted}</br>`,
      `You received ${valueMessage}!`, // toast header
      toastOptions
    );
  }

  private formatFromText(val: string): string {
    const aestheticToastFromLength = 16; // TODO: Check the notiification width and use that if possible
    const len = val.length;
    if (len > aestheticToastFromLength) {
      return val.substring(0, aestheticToastFromLength) + "...";
    }
    return val;
  }
}

export default function<T>(
  arr: T[],
  keyDerivePredicate: (x: T) => number
): T[] {
  const uniqKeys: { [key: number]: number } = {};

  if (!Array.isArray(arr)) {
    return [];
  }

  const uniqArr = arr.filter((item: T) => {
    const key = keyDerivePredicate(item);
    if (!uniqKeys[key]) {
      uniqKeys[key] = key;
      return item;
    }
    return;
  });

  return uniqArr;
}

import Vue from "vue";

import BaseModal from "./BaseModal.vue";
import BaseSelect from "./BaseSelect.vue";
import BaseInput from "./BaseInput.vue";

export default () => {
  Vue.component("BaseModal", BaseModal);
  Vue.component("BaseSelect", BaseSelect);
  Vue.component("BaseInput", BaseInput);
};

import { VueConstructor } from "vue";

export default interface ModalStorePayload {
  modal: VueConstructor;
  attr: object;
}

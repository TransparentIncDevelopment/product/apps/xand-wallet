export default interface BankInfo {
  name: string;
  logo: string;
}

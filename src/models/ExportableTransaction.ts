import { OperationType, Transaction } from "xand-member-api-client";
import displayMessages from "@/constants/displayMessages";
import moment from "moment";
import Money from "tpfs-money";

function formatCSV(input: string): string {
  return `"${input.replace(`"`, `""`)}"`;
}

function localizeDateTime(utcDateTime: string): string {
  return moment
    .utc(utcDateTime)
    .local()
    .toISOString(true);
}

export default class ExportableTransaction {
  public txnId: string = "";
  public dateTime: string = "";
  public txnTypeLabel: string = "";
  public amount: string = "";

  // exist for transfers only
  public fromAddress: string = "";
  public toAddress: string = "";

  // exist for create/redeems only
  public routingNumber: string = "";
  public bankName: string = "";
  public accountNumber: string = "";

  public state: string = "";

  public static header(): string {
    const columns: string[] = [
      "Date/Time",
      "Txn Type",
      "To",
      "From",
      "Txn Amount",
      "Bank Name",
      "Routing Number",
      "Acct Number",
      "Txn ID",
      "Txn State"
    ];
    return columns.join(",");
  }

  public toString(): string {
    const fields: string[] = [
      formatCSV(this.dateTime),
      formatCSV(this.txnTypeLabel),
      formatCSV(this.toAddress),
      formatCSV(this.fromAddress),
      formatCSV(this.amount),
      formatCSV(this.bankName),
      formatCSV(this.routingNumber),
      formatCSV(this.accountNumber),
      formatCSV(this.txnId),
      formatCSV(this.state)
    ];

    const record = fields.join(",");
    return record;
  }

  public static fromTransaction(
    input: Transaction,
    memberAddress: string
  ): ExportableTransaction {
    const result = new ExportableTransaction();

    result.txnId = input.transactionId || "";
    result.dateTime = localizeDateTime(input.datetime);
    result.amount = Money.displayCurrency(
      "en-US",
      "USD",
      input.amountInMinorUnit
    );
    result.state = input.status.state;

    result.bankName = input.bankAccount
      ? (input.bankAccount.bankName as string)
      : "";
    result.routingNumber = input.bankAccount
      ? (input.bankAccount.routingNumber as string)
      : "";
    result.accountNumber = input.bankAccount
      ? (input.bankAccount.maskedAccountNumber as string)
      : "";

    switch (input.operation) {
      case OperationType.CreationRequest.toString():
        result.txnTypeLabel = displayMessages.txHistory.transferToWallet;
        break;
      case OperationType.Redeem.toString():
        result.txnTypeLabel = displayMessages.txHistory.transferToBank;
        break;
      case OperationType.Payment.toString():
        if (input.signerAddress === memberAddress) {
          result.txnTypeLabel = displayMessages.txHistory.paymentSent;
        } else {
          result.txnTypeLabel = displayMessages.txHistory.paymentReceived;
        }
        result.fromAddress = input.signerAddress || "";
        result.toAddress = input.destinationAddress || "";
        break;
    }

    return result;
  }
}

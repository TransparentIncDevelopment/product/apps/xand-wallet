import EnrichedTransaction from "./EnrichedTransaction";

export default interface TransactionPageView {
  enrichedTxList: EnrichedTransaction[];
  currentPage: number;
  length: number;
}

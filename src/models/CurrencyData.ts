export default interface CurrencyData {
  name: string;
  symbol: string;
}

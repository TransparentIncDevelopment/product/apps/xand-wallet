export default interface UserDetails {
  address: string;
  registered: boolean;
  userBalanceInMinorUnit: number;
  lastRefreshedDateTime: Date | string;
}

declare global {
  interface Window {
    __env: any;
    require: any;
  }
}

const setEnv = (window: Window) => {
  window.__env = window.__env || {};

  /* Deployment Environment : LOCAL / DEV / TESTNET / MONEYMONEY */
  if (window.require) {
    // Yet another "don't do this in test" hack.
    window.__env.DEPLOYMENT = process.env.XAND_WALLET_ENV || "LOCAL";
  } else {
    window.__env.DEPLOYMENT = "LOCAL";
  }

  /* Constant common for all environment */
  window.__env.USERKEY_CONSTANT = {
    ADDRESS_LENGTH: 2
  };
  /* Bank Account Id Filters */
  window.__env.BANK_ACCOUNT_ID_FILTER = [];

  /* Constant specific to environment */
  if (window.__env.DEPLOYMENT === "LOCAL") {
    window.__env.MEMBER_API_URL = "http://localhost:3000";
  } else if (window.__env.DEPLOYMENT === "DEV") {
    window.__env.MEMBER_API_URL = "https://member-api.dev.xand.tools";
  } else if (window.__env.DEPLOYMENT === "TESTNET") {
    window.__env.MEMBER_API_URL = "http://test-net-member-api.xand.to";
  } else if (window.__env.DEPLOYMENT === "PROD") {
    window.__env.MEMBER_API_URL = "https://member-api.xand.network";
  }

  // Require was annoyingly broken on purpose during testing so this lame hack just avoids
  // problems with that.
  if (window.require) {
    const electron = window.require("electron");

    window.__env.certStoragePath = (
      electron.app || electron.remote.app
    ).getPath("userData");
  }
};

setEnv(window);

const env = window.__env;

export default env;

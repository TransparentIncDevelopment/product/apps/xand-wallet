import { Currency } from "tpfs-money";

import CurrencyData from "@/models/CurrencyData";

const currencyDataTable: Array<[Currency, CurrencyData]> = [
  ["USD", { name: "United States Dollar", symbol: "$" }],
  ["EUR", { name: "Euro", symbol: "€" }]
];

export default currencyDataTable;

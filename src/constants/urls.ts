export default {
  CREATE_USERKEY: "/api/createuser",
  REGISTER_USERKEY: "/api/register",
  GET_TXN_HISTORY: "/api/txnhistory"
};

export const loggerIndex = {
  ERR_: {
    SER0001: "SER0001 : ",
    SER0002: "SER0002 : ",
    SER0003: "SER0003 : ",
    SER0004: "SER0004 : ",
    SER0005: "SER0005 : ",
    SER0006: "SER0006 : ",
    SER0007: "SER0007 : ",
    SER0008: "SER0008 : ",
    SER0009: "SER0009 : ",
    SER0010: "SER0010 : ",
    SER0011: "SER0011 : ",
    SER0012: "SER0012 : ",
    SER0013: "SER0013 : ",
    SER0014: "SER0014 : ",
    SER0015: "SER0015 : ",
    SER0016: "SER0016 : ",
    SER0017: "SER0017 : ",
    SER0018: "SER0018 : ",
    SER0019: "SER0019 : ",
    SER0020: "SER0020 : ",
    SER0021: "SER0021 : ",
    SER0022: "SER0022 : ",
    SER0023: "SER0023 : ",
    SER0024: "SER0024 : ",
    SER0025: "SER0025 : ",
    SER0026: "SER0026 : ",
    SER0027: "SER0027 : ",
    SER0028: "SER0028 : ",
    SER0029: "SER0029 : ",
    SER0030: "SER0030 : ",
    SER0031: "SER0031 : ",
    SER0032: "SER0032 : ",
    SER0033: "SER0033 : ",
    SER0034: "SER0034 : ",
    SER0035: "SER0035 : ",
    SER0036: "SER0036 : ",
    SER0037: "SER0037 : ",
    SER0038: "SER0038 : ",
    SER0039: "SER0039 : ",
    SER0040: "SER0040 : ",
    SER0041: "SER0041 : ",
    SER0042: "SER0042 : ",
    SER0043: "SER0043 : ",
    SER0044: "SER0044 : ",
    SER0045: "SER0045 : ",
    SER0046: "SER0046 : ",
    SER0047: "SER0047 : ",
    SER0048: "SER0048 : ",
    SER0049: "SER0049 : ",
    SER0050: "SER0050 : ",
    SER0051: "SER0051 : ",
    SER0052: "SER0052 : ",
    SER0053: "SER0053 : ",
    SER0054: "SER0054 : ",
    SER0055: "SER0055 : ",
    SER0056: "SER0056 : ",
    SER0057: "SER0057 : ",
    SER0058: "SER0058 : "
  },
  WARN_: {
    SER0001: "SER0001 : ",
    SER0002: "SER0002 : ",
    SER0003: "SER0003 : ",
    SER0004: "SER0004 : ",
    SER0005: "SER0005 : ",
    SER0006: "SER0006 : ",
    SER0007: "SER0007 : ",
    SER0008: "SER0008 : ",
    SER0009: "SER0009 : ",
    SER0010: "SER0010 : "
  },
  INFO_: {
    SER0001: "SER0001 : ",
    SER0002: "SER0002 : ",
    SER0003: "SER0003 : ",
    SER0004: "SER0004 : ",
    SER0005: "SER0005 : ",
    SER0006: "SER0006 : ",
    SER0007: "SER0007 : ",
    SER0008: "SER0008 : ",
    SER0009: "SER0009 : ",
    SER0010: "SER0010 : ",
    SER0011: "SER0011 : ",
    SER0012: "SER0012 : ",
    SER0013: "SER0013 : ",
    SER0015: "SER0015 : ",
    SER0016: "SER0016 : ",
    SER0017: "SER0017 : ",
    SER0018: "SER0018 : ",
    SER0021: "SER0021 : ",
    SER0022: "SER0022 : "
  }
};

export const loggerMessage = {
  // potpourri
  UNDEFINED_MSG: "Expected value is undefined",
  SEPARATOR_MSG: " : ",
  FILE_SELECTED: "File selected path is : ",

  // clipboard
  PUBLIC_ADDRESS_COPIED: "Address copied to clipboard and copied value is : ",
  PUBLIC_ADDRESS_COPYING_FAILED: "Failed to copy Address to clipboard",
  TRANSACTION_COPIED:
    "Transaction ID copied to clipboard and copied value is :",
  TRANSACTION_COPYING_FAILED: "Failed to copy transaction id to clipboard",

  // transfer modal validation
  INVALID_TRANSFER_REQUEST:
    "Cannot complete transfer process because of invalid amount",
  ZERO_REQ_AMT: "Request amount was a zero value",

  // transaction/balance updates
  UPDATE_BALANCES_FAILED: "Timeout while updating both account balances",
  TRANSACTION_STATUS_FETCH_MANUALLY:
    "Transaction status is fetched manually for transaction id : ",
  TRANSACTION_STATUS_FETCH_SUCCESSFULLY:
    "Transaction status is fetched successfully for transaction id : ",
  TRANSACTION_STATUS_FETCH_INVALID:
    "Transaction status fetched is invalid for transaction id : ",
  TRANSACTION_STATUS_FETCH_UNKNOWN:
    "Transaction status fetched is unknown for transaction id : ",
  TRANSACTION_STATUS_FETCH_PENDING:
    "Transaction status fetch is pending for transaction id : ",

  // contacts
  CONTACT_MISSING_CO:
    "Contact missing company. Please re-enter this information",
  CONTACT_INVALID_ADDRESS: "Address for contact is invalid or missing",
  CONTACT_STATE_UPSERT_FAILED: "Could not update contacts data in state",
  CONTACT_STATE_DELETE_FAILED: "Could not delete contacts data in state",
  CONTACT_FILE_READING_FAILED:
    "Failed to read the existing contact file! Please contact support@xand.me",
  CONTACT_FILE_WRITING_FAILED:
    "Failed to write data into existing contact file! Please contact support@xand.me"
};

const supportMsg = "Please contact ";
const supportEmail = "support@xand.me";

export default {
  // templates and regex
  LOCALIZED_DATETIME_FORMAT: "MM/DD/YY hh:mm:ss a",

  // potpourri
  EMPTY_MSG: "",
  BLANK_SPACE_MSG: " ",
  STANDARD_NETWORK_ERROR_MSG:
    "A network error occurred. Please contact " + supportEmail,
  STANDARD_500_REQUEST_FAILED_MEMBER_API_ERROR:
    "Unexpected network error. Please try again or contact your system admin.",
  POLL_FAIL_MAX_RETRY:
    "It is taking longer than normal to confirm your transaction. If you do not see the transaction shortly, please contact " +
    supportEmail,
  ERROR_APP_LAUNCH:
    "Error in launching wallet app! " + supportMsg + supportEmail,
  ADDRESS_NOT_FOUND: "Could not connect to Member API",

  // transaction/balance updates
  ACCOUNT_BANK_BALANCE_FETCH_STUCK:
    "Could not fetch bank balance! " + supportMsg + supportEmail,
  BALANCE_FETCHING_STUCK:
    "Could not fetch latest wallet balance! " + supportMsg + supportEmail,
  WAITING_SEND_STATUS: "Awaiting confirmation",
  SUCCESSFUL_SEND_STATUS: "Successful Transaction",
  FAILED_SEND_STATUS: "Transaction Failed",
  REFRESH_SEND_STATUS: "Try refreshing manually",
  SEND_POLL_FAILURE:
    "It is taking longer than normal to confirm your transaction. If you do not see the transaction shortly, please contact " +
    supportEmail,
  REDEEM_WALLET_SUCCESS:
    "Success! Funds have been transferred to your bank account",
  BALANCES_UPDATING: "Your balances are being updated...",
  LOAD_WALLET_SUCCESS: "Success! Funds have been transferred to your wallet",
  LOAD_WALLET_SUCCESS_TIMEOUT: 4500,
  TRANSACTION_STATUS_STUCK:
    "It is taking longer than normal to confirm your transaction. Please refresh manually or contact support at " +
    supportEmail,
  BALANCE_UPDATE_FAILED:
    "Could not refresh balances. " + supportMsg + supportEmail,

  // transfer validation
  INVALID_LENGTH_RECEIVER_ADDRESS: "Recipient address has invalid length",
  INVALID_OWN_ACCOUNT_TRANSFER:
    "Transfer to own account is not allowed! Please enter a different destination",
  RECEIVER_ADDRESS_MANDATORY:
    "Receiver's address is mandatory! Please enter a value",
  VALID_AMOUNT: "Entered amount is valid",
  INSUFFICIENT_BALANCE: "Insufficient balance",
  ZERO_AMOUNT: "Amount cannot be zero",
  INVALID_AMOUNT: "Invalid amount",
  AMOUNT_MANDATORY: "You must enter an amount",
  DESTINATION_MANDATORY: "You must select a transfer destination",
  BANK_TRANSFER_FAILED: "Could not make reserve bank transfer",

  txHistory: {
    transferPending: "Transfer Pending",
    transferCancelled: "Transfer Cancelled",
    transferToWallet: "Transfer to Wallet",
    transferToBank: "Transfer to Bank",
    transfer: "Transfer",
    paymentSent: "Payment Sent",
    paymentReceived: "Payment Received",
    registration: "Registration"
  },

  // clipboard
  TRANSACTION_COPIED: "Transaction ID copied to clipboard",
  PUBLIC_ADDRESS_COPIED: "Address copied to clipboard",

  // contacts
  CONTACT_UPDATE_NOT_NECESSARY: "Already updated",
  CONTACT_EXIST:
    "Contact with same address already exists! Please re-enter your information",
  CONTACT_FILE_DELETION_FAILED:
    "Failed to delete contact file! " + supportMsg + supportEmail,
  CONTACT_ADDITION_SUCCESSFUL: "New contact added successfully ",
  CONTACT_ADDITION_SUCCESSFUL_TIMEOUT: 3000,
  CONTACT_UPDATE_SUCCESSFUL: "Contact updated successfully",
  CONTACT_UPDATE_SUCCESSFUL_TIMEOUT: 3000,
  CONTACT_DELETE_SUCCESSFUL: "Contact deleted successfully",
  CONTACT_DELETE_SUCCESSFUL_TIMEOUT: 3000,

  // account validation
  EXCESS_REDEEM_AMOUNT: "Amount entered exceeds available wallet balance.",
  LOAD_EXCEEDS_FUNDS: "Amount entered exceeds available bank balance.",
  DEFAULT_BANK_NOT_SET:
    "Bank balance will appear once you have made a transfer and selected a bank/account",
  MANDATORY_FIELD:
    "Company Name and Address are required! Please enter missing information",
  INVALID_ADDRESS: "Invalid address! Please confirm and re-enter your address",

  // authentication
  INVALID_JWT:
    "JWT Authorization Failed. Please verify you've configured the correct JWT or contact " +
    supportEmail,
  NO_JWT_SET: "No JWT Authorization set for wallet",
  BANK_AUTH_ERROR:
    "Could not authenticate with bank. " + supportMsg + supportEmail,
  KEY_NOT_REGISTERED:
    "The provided user key is not registered! Please check that the key is correct."
};

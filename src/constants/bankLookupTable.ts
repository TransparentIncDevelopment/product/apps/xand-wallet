import * as OctopodaBankLogo from "@/assets/images/OctopodaBank.svg";
import * as ChamberedTrustLogo from "@/assets/images/ChamberedTrust.svg";
import * as TentacleFinancialLogo from "@/assets/images/TentacleFinancial.svg";
import * as mcbLogo from "@/assets/images/mcb_logo.png";
import * as silvergateLogo from "@/assets/images/silvergate_logo.png";
import * as providentLogo from "@/assets/images/provident_logo.png";
import BankInfo from "@/models/BankInfo";

const bankLookupTable: Array<[string, BankInfo]> = [
  ["800000002", { name: "Chambered Trust", logo: ChamberedTrustLogo }],
  ["800000001", { name: "Octopoda Bank", logo: OctopodaBankLogo }],
  ["800000003", { name: "Tentacle Financial", logo: TentacleFinancialLogo }],
  ["928173892", { name: "Silvergate", logo: silvergateLogo }],
  ["121141343", { name: "Metropolitan Commercial Bank", logo: mcbLogo }],
  ["211374020", { name: "Provident", logo: providentLogo }]
];

export default bankLookupTable;

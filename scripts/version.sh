#!/bin/bash
set -e         # exit early on error
set -x         # print commands before executing
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail # abort if command in pipeline fails

echo Verifying version $VERSION has not already been published.

RESPONSE=$(curl -s -o /dev/null -w "%{http_code}" -u"$ARTIFACTORY_USER:$ARTIFACTORY_PASS" "https://transparentinc.jfrog.io/transparentinc/artifacts-external/xand-wallet/${VERSION}/")

if [[ $RESPONSE -eq 200 ]]; then
    echo Version $VERSION has already been published -- please remember to bump your versions.
    exit 1
else
  exit 0
fi

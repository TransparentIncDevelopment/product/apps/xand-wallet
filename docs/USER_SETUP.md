# Xand Wallet User Setup

## Contents

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Installation](#installation)
  - [Windows](#windows)
  - [Mac OS](#mac-os)
  - [Linux](#linux)
- [Initial Setup](#initial-setup)
  - [Prerequisites](#prerequisites)
  - [Wallet Address Import](#wallet-address-import)
  - [API URL & JWT Authentication](#api-url--jwt-authentication)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Installation

### Windows

1. Download the `xand-wallet-windows-{x.y.z}.exe` file
1. Run the exe installer
1. After the wallet installer has run, launch the program if it does not open automatically

### Mac OS

1. Download the file `xand-wallet-mac-{x.y.z}.dmg` file
1. Run the dmg file, and a window will pop up with the xand-wallet icon and the `Applications` folder
1. Drag the xand-wallet icon into the `Applications` folder in the popped up window
1. Launch the wallet from `Applications`

### Linux

1. Download the `xand-wallet-linux-{x.y.z}.AppImage` file
1. Make the file executable with the command: `chmod a+x xand-wallet-linux-{x.y.z}.AppImage`
1. Run the wallet from the command line: `./xand-wallet-linux-{x.y.z}.AppImage`

## Initial Setup

### Prerequisites

Before continuing you should have the following:

1. The URL of the API your wallet software is going to use
1. The JWT token your target API uses for authentication (if applicable)
1. Your wallet address file, which should be a file with contents of the format `ADDRESS={your_xand_public_address}`

### Wallet Address Import

![Import Page](img/import.png)

Launching the wallet software without a wallet address imported will bring up the import screen.  Press the "Import Address / User Key" button and choose to import your wallet address file.  This will load the software with your wallet information.


### API URL & JWT Authentication

Once the wallet software has loaded the address, click on the top-left user icon to open the Account Information window.  Enter the URL of your target API *with no trailing slash* and (if applicable) the JWT token into the appropriate fields.  When you are done press "Save" and the software will refresh with the given settings, ready to use.

![Import Page](img/accountinfo.png)
![Import Page](img/apigiven.png)
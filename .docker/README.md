# About the Wallet-Builder CI Image

This `Dockerfile` is used to build `xand-wallet` in CI systems.

This image is _not_ published by gitlab. It is manually updated
and published as needed.

It is maintained by `xand-wallet` engineers.

## Workflow to Update the Build Image

1. `make artifacts` to copy relevant working files to the `tmp` directory.
1. `make build` to build the image.
    * If you intend to publish a new version of the image, please update its version number in the Makefile.
1. `make run` to get into the image.
1. Inside the running image, `cd /app` to get to where the source code is.
1. `yarn install && yarn build` to build the wallet in the container.
1. `make publish` to push the image to GCR so that it is available for gitlab to use.

## Notes

1. The `Makefile` assumes you have your `.npmrc` in your `~/` directory. It copies this file
into the `tmp` dir so that you can build the src inside the locally running container.
#!/bin/bash

set -e         # exit early on error
set -x         # print commands before executing
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail # abort if command in pipeline fails

echo Verifying $1:$2 does not already exist in the registry.

exit $(gcloud container images list-tags $1 --filter="tags~^$2$" --format json | jq '.[] | if length > 0 then 1 else 0 end')

### TESTS
### ./check_publish
###  -> should error out with unbound variable

### ./check_publish IMAGE NOT_EXISTING_TAG
### echo $?
### 0

### ./check_publish IMAGE EXISTING_TAG 
### echo $?
### -1
